@extends('layouts.master')
@section('content')
<div class="container">
@include('flash::message') 
  <h2>Quản lí người dùng</h2>      
  <a href=""><button class="btn btn-primary col-md-1">Thêm</button></a> 
  <br>   
  <br>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>STT</th>
        <th>Username</th>
        <th>Role</th>
        <th>Sửa</th>
        <th>Xóa</th>
      </tr>
    </thead>
    @foreach($users as $key => $user)
    <tbody>
      <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $user->username }}</td>
        <td>{{ $user->role }}</td>
        <td><a href="{{ route('admin_users.edit', $user->id) }}" class="btn btn-primary" role="button">Edit</a></td>
        <td><a href="{{ route('admin_users.delete', $user->id) }}" class="btn btn-danger" role="button">Delete</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
    {{$users->links()}}
</div>
@endsection