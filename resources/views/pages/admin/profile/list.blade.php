@extends('layouts.master')
@section('content')
<div class="container">
@include('flash::message') 
  <h2>Quản lí hồ sơ</h2>      
  <a href=""><button class="btn btn-primary col-md-1">Thêm</button></a> 
  <br>   
  <br>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>STT</th>
        <th>Tên</th>
        <th>Email</th>
        <th>SĐT</th>
        <th>Quê quán</th>
        <th>Chuyên ngành</th>
        <th>Học vấn</th>
        <th>EXP</th>
        <th>Ngày sinh</th>
        <th>Sửa</th>
        <th>Xóa</th>
      </tr>
    </thead>
    @foreach($profiles as $key => $profile)
    <tbody>
      <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $profile->name }}</td>
        <td>{{ $profile->email }}</td>
        <td>{{ $profile->sdt }}</td>
        <td>{{ $profile->que_quan }}</td>
        <td>{{ $profile->chuyen_nganh }}</td>
        <td>{{ $profile->hoc_van }}</td>
        <td>{{ $profile->exp_year }}</td>
        <td>{{ $profile->birth_day }}</td>
        <td><a href="{{ route('admin_profiles.edit', $profile->id) }}" class="btn btn-primary" role="button">Edit</a></td>
        <td><a href="{{ route('admin_profiles.delete', $profile->id) }}" class="btn btn-danger" role="button">Delete</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
    {{$profiles->links()}}
</div>
@endsection