<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

class ProfileController extends Controller
{
    protected $profile;
    // ham khoi tao
    public function __construct(Profile $profile) {
        $this->profile = $profile;
    }
    public function index()
    {
        return view('pages.admin.index');
    }

    public function listProfile() {
        $profiles = $this->profile->getProfiles();
        return view('pages.admin.profile.list', compact('profiles'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editUser($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $success = $this->profile->deleteProfile($id);
        if ($success) {
            flash('Bạn đã xóa thành công !')->success();
        } else {
            flash('Đã xảy ra lỗi !')->error();
        }
        return redirect()->route('admin_profiles.list');
    }
}
