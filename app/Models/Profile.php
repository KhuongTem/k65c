<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Profile extends Authenticatable
{
    use Notifiable;

    public $table = "profiles";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'sdt','que_quan','chuyen_nganh','hoc_van','exp_year','birth_day',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getProfiles() {
        $profiles = Profile::paginate(20);
        return $profiles; 
    }

    /**
     * delete user 
     *
     * @param int $id
     * @return boolean
     */
    public function deleteProfile($id)
    {
        return Profile::find($id)->delete();
    }
}
