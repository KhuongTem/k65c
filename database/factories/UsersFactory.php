<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\User;
use App\Model;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'username' => $faker->username,
        'password' => Hash::make(12345678),
        'role' => rand(1,2),
    ];
});
