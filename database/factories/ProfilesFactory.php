<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Profile;
use App\Model;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

$factory->define(App\Models\Profile::class, function (Faker $faker) {
    return [
        'name' => $faker->username,
        'email' => $faker->email,
        'sdt' => '0967123123',
        'que_quan' => $faker->address,
        'chuyen_nganh' => $faker->name,
        'hoc_van' => $faker->name,
        'exp_year' => rand(4,5),
        'birth_day' => $faker->dateTime(),
    ];
});
