<?php

use Illuminate\Database\Seeder;
use App\Models\Profile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Profile::truncate();
        factory(Profile::class, 100)->create();
        Schema::enableForeignKeyConstraints();
    }
}
