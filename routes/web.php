<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('admin', 'UserController@index');
Route::get('admin/user/list', 'UserController@listUser')->name('admin_users.list');
Route::get('admin/user/edit/{id}', 'UserController@editUser')->name('admin_users.edit');
Route::get('admin/user/delete/{id}', 'UserController@destroy')->name('admin_users.delete');
Route::get('admin/profile/list', 'ProfileController@listProfile')->name('admin_profiles.list');
Route::get('admin/profile/edit/{id}', 'ProfileController@listProfile')->name('admin_profiles.edit');
Route::get('admin/profile/delete/{id}', 'ProfileController@destroy')->name('admin_profiles.delete');
Route::get('admin/company/list', 'UserController@listUser')->name('admin_companies.list');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
